from functools import cmp_to_key

#a) I
a = [-1, -2, 0, 4, 2, -10, 5, 13, 9]
print(sorted(a, key=abs))

#a) II
b = [("one", 1), ("two", 2), ("four", 4), ("three", 3)]
print(sorted(b, key=lambda pair: pair[1]))

#b)
def myCompare(x, y):
    if x%2 == y%2:
        if x%2==0:
            return x - y
        else:
            return y - x
    elif x%2 == 0:
        return -1
    return 1

c = [-1, -2, 13, 9, 0, 4, 14, 2, -10, 5]

print(sorted(c, key=cmp_to_key(myCompare))) # using compare
print(sorted(c, key=lambda x: [x%2, x if x%2 == 0 else -x])) # using key (recommended)

