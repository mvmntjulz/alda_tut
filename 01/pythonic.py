# one line list creation
# =============================================================
list_1 = list(range(1, 20, 3))
list_2 = [x for x in list_1 if x % 2 == 0] 
print(f"list_1: {list_1}")
print(f"list_2: {list_2}")


# one line if else
# =============================================================
list_2[0] = 1337 if True else 1338
print(f"after if else: {list_2}")


# direct element iteration
# =============================================================
for elem in list_2:
    print(elem+2)


# direct element iteration + indices
# =============================================================
for i, elem in enumerate(list_2):
    print(i, elem)


# Pythons call by assignment
# =============================================================
def foo(bar):
    bar.append(42)
    print(f"foo(bar): {bar}")

answer_list = list() 
foo(answer_list)
print(f"after foo(bar): {answer_list}")
# >> [42]

# BUT:
def foo2(bar):
    bar = 6 
    print(f"foo2(bar): {bar}")
    # >> new value

five = 5 
foo2(five)
print(f"after foo2(bar): {five}")
# >> "old value"

# BUT:
list_3 = [3,4,5]
tup = (list_3, "Ayaya")
print(f"before append: {tup}")
list_3.append(6)
print(f"after append: {tup}")
