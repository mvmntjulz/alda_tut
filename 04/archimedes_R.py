#Loesungsskizze Blatt 3 Aufgabe 2

from math import sqrt

def archimedes1(k):
    s,t,n = sqrt(2),2,4 #Startwerte: Quadrat 
    print ('n:',n,' s:', n/2*s,'< pi < t:',n/2*t,'Dif:',n/2*t-n/2*s)
    for i in range(0,k):
        if abs(t - 2.0*s / sqrt(4.0 - s**2)) < 1e-15: #Test
            n *= 2
            s = sqrt(2.0 - sqrt(4.0 - s**2))
            t = 2.0 / t * (sqrt(4.0 + t**2) - 2.0)
            print ('n:',n,' s:', n/2*s,'< pi < t:',n/2*t,'Dif:',n/2*t-n/2*s)
        else:
           print ('Fehler bei n =', n)
           break


def archimedes2(k):
    s,t,n = sqrt(2),2,4 #Startwerte: Quadrat 
    print ('n:',n,' s:', n/2*s,'< pi < t:',n/2*t,'Dif:',n/2*t-n/2*s)
    for i in range(0,k):
        if abs(t - 2.0*s / sqrt(4.0 - s**2)) < 1e-15: #Test
            n *= 2
            s = s / sqrt(2.0 + sqrt(4.0 - s**2))
            t = (2.0*t) / (sqrt(4.0 + t**2) + 2.0)
            print ('n:',n,' s:', n/2*s,'< pi < t:',n/2*t,'Dif:',n/2*t-n/2*s)
        else:
           print ('Fehler bei n =', n)
           break

def archimedes1_R(i, k, s, t, n):
    if k == i:
        return s, t, n
    if abs(t - 2.0*s / sqrt(4.0 - s**2)) < 1e-15: #Test
        n *= 2
        s = s / sqrt(2.0 + sqrt(4.0 - s**2))
        t = (2.0*t) / (sqrt(4.0 + t**2) + 2.0)
        print ('n:',n,' s:', n/2*s,'< pi < t:',n/2*t,'Dif:',n/2*t-n/2*s)
        i += 1
        archimedes1_R(i, k, s, t, n)
    else:
        print ('Fehler bei n =', n)
        #return s, t, n

print ("original implementation")
#archimedes1(30)
print ("improved implementation")
archimedes2(30)
print ("improved recursive implementation")
archimedes1_R(0, 30, sqrt(2), 2, 4)
