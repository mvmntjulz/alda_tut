Bergründung 2 a):
sieve2(k) prüft vor der inneren Schleife zunächst, ob k eine primzahl ist. Falls nämlich primes[k] == 0 ist, so wird man nie vielfache von diesem k streichen, da k selbst vielfaches von Primzahlen kleiner als k selbst ist und somit
alle potenziell zu streichenden Werte bereits durch die kleineren Primzahlen passiert ist.
Bergründung 2 d):
Man kann bei sqrt(N) aufhören, da jede Primzahl p, welche größer sqrt(N) ist, mit einer Zahl größer p multipliziert werden müsste, um Zahlen zu streichen, die noch nicht gestrichen wurden.
Da aber p*q > N für q > p gilt, gibt es solche vielfache von p innerhalb von N nicht. Die Multiplikation mit Zahlen kleiner p und die daraus entstehenden Vielfachen wurde ja schon alle vorher gestrichen.
